﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSlidingDoorBetter_Com : MonoBehaviour {

    AudioSource audioSource;

    public float doorOpenAmount = 1f;
    public Transform Door1Tran;
    public Transform Door2Tran;
    public float speed = 5f;
    public float snapDistance = 0.01f;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 1f);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    IEnumerator DoorMove(float target)
    {
        audioSource.Play();
        float xPos = Door1Tran.localPosition.x;
        while (xPos < (target - snapDistance) || xPos > (target + snapDistance))
        {
            xPos = Mathf.Lerp(Door1Tran.localPosition.x, target, Time.deltaTime * speed);
            Door1Tran.localPosition = new Vector3(xPos, 0, 0);
            Door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Moving");
            yield return null;
        }
        Door1Tran.localPosition = new Vector3(target, 0, 0);
        Door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Stopped");
        yield return null;
    }
}
