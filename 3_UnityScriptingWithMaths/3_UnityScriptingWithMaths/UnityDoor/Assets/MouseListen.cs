﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseListen : MonoBehaviour {
    public bool mouseCursorOn;
    public bool mouseClicked;


    private void OnMouseDown()
    {
        mouseClicked = true;
    }

    private void OnMouseUp()
    {
        mouseClicked = false;
    }

    private void OnMouseOver()
    {
        if (mouseCursorOn == false)
        {
            mouseCursorOn = true;
        }
    }

    private void OnMouseExit()
    {
        mouseCursorOn = false;
    }
}
