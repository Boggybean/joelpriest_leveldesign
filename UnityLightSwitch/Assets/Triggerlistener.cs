﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggerlistener : MonoBehaviour
{
    public bool playerEntered = false;

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }

}
