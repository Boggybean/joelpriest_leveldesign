﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPickupScript : MonoBehaviour
{
    public TriggerListener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    public MeshRenderer meshRend;
    public bool keyPickedUp = false;
    public BoxCollider redKey;
    public SphereCollider redKeyCollision;



	// Use this for initialization
	void Start ()
    {
        audSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();
	}

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.enabled = false;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.enabled = false;
        }
    }
    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            meshRend.enabled = false;
            redKey.enabled = false;
            redKeyCollision.enabled = false;
            keyPickedUp = true;
        }
    }
}

