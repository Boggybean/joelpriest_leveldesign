﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RedKeyPlaced : MonoBehaviour {
    public Image cursorImage;
    public TriggerListener_Com trigger;
    public KeyPickupScript redKey;
    public MeshRenderer keyPlaced;
    public Light Light1;
    public Light Light2;
    public Light Light3;
    public Light Light4;
    public Light Light5;
    public Light Light6;
    public Light Light7;
    public Light Light8;
    public Light Light9;
    public Light Light10;
    public Light Light11;
    public Light Light12;
    public Light Light13;
    public Light Light14;
    public Animation DoorOpen;
    public bool keyDone;

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                //screenText.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
           // screenText.enabled = false;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
           // screenText.enabled = false;
        }
    }
    void OnMouseDown()
    {
        if (redKey.keyPickedUp == true)
        {
            keyPlaced.enabled = true;
            keyDone = true;
            Light1.enabled = true;
            Light2.enabled = true;
            Light3.enabled = true;
            Light4.enabled = true;
            Light5.enabled = true;
            Light6.enabled = true;
            Light7.enabled = true;
            Light8.enabled = true;
            Light9.enabled = true;
            Light10.enabled = true;
            Light11.enabled = true;
            Light12.enabled = true;
            Light13.enabled = true;
            Light14.enabled = true;
            DoorOpen.enabled = true;
        }
    }
}
