﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour {
    public TriggerListener_Com trigger;
    public Text EndText;

    void OnTriggerEnter(Collider trigger)
    {
        EndText.enabled = true;
    }


}
